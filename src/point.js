import * as R from 'ramda';
import { maybe } from 'maybes';
import { multiply as matrixMultiply } from 'matrix';

import * as Transform from './transform';

const { curry, compose } = R;

export const zero = { x: 0, y: 0 };
Object.freeze(zero);

export const unit = { x: 1, y: 1 };
Object.freeze(unit);


export const map = curry((f, {x, y}) => ({x: f(x), y: f(y)}));

// reduce :: ((x, y) -> a) -> Point -> a
export const reduce = curry((reducer, {x, y}) => reducer(x, y));

export const squaredMagnitude = compose(reduce((a, b) => a + b), map((n) => n * n));
export const magnitude = compose(Math.sqrt, squaredMagnitude);

export const normalize = (v) => maybe(magnitude(v)).map((m) => scale(1 / m)(v)).orJust(undefined);


// Transform this vector by a 3x3 affine transform matrix (row-major).
// transform :: matrix -> Point -> Point'
export const transform = curry((matrix, pt) => {
	const transformedPointAsMatrix = matrixMultiply(matrix, [[pt.x], [pt.y], [1]]);
	return {x: transformedPointAsMatrix[0][0], y: transformedPointAsMatrix[1][0]};
});


// translate :: Point -> Point -> Point'
export const translate = R.uncurryN(2, (offset) => transform(Transform.translation(offset)));

// rotate :: angle -> Point -> Point'
export const rotate = R.uncurryN(2, (angle) => transform(Transform.rotation(angle)));

// scale :: Number -> Point -> Point'
export const scale = R.uncurryN(2, (scaleFactor) => transform(Transform.scaling({ x: scaleFactor, y: scaleFactor })));


export const add = translate;
export const subtract = curry((v1, v2) => add(v1)(negate(v2)));
export const multiply = curry((v1, v2) => ({ x: v1.x * v2.x, y: v1.y * v2.y }));
export const piecewiseDivide = curry((v1, v2) => ({ x: v1.x / v2.x, y: v1.y / v2.y }));

export const negate = scale(-1);

// pointInside :: { rect :: Rect, applying :: Transform? } -> Point -> Bool
export const pointInside = curry(({ rect, applying: xf = Transform.identity }, point) => {
	const { x, y } = transform(Transform.invert(xf))(point);
	return x >= rect.origin.x
		&& x <= (rect.origin.x + rect.size.x)
		&& y >= rect.origin.y
		&& y <= (rect.origin.y + rect.size.y);
});

export const dot = curry((v1, v2) =>
	v1.x * v2.x + v1.y * v2.y);

export const angleBetween = curry((v1, v2) =>
	Math.atan2(v2.y - v1.y, v2.x - v1.x));

// transformSize :: Transform -> Point -> Point
export const transformSize = curry((xf, size) => {
	const [a, b, c] = [
		add(zero)({ x: size.x, y: 0 }),
		zero,
		add(zero)({ x: 0, y: size.y })
	].map(transform(xf));
	return {
		x: distanceBetween(a, b),
		y: distanceBetween(b, c),
	};
});

// -- Point / coordinate systems

// convertClientPoint :: { getBoundingClientRect :: () -> DOMRect } -> Point -> Point
export const convertClientPoint = R.uncurryN(2, (target) => convertClientPointToBounds(target.getBoundingClientRect()));

// convertClientPointToBounds :: DOMRect -> Point -> Point
export const convertClientPointToBounds = curry((bounds, { x, y }) => {
	return { 
		x: x - bounds.left,
		y: y - bounds.top,
	};
});

// distanceBetween :: Point -> Point -> Number
// distanceBetween :: (Point, Point) -> Number
export const distanceBetween = curry((p1, p2) => magnitude(subtract(p1)(p2)));

// isEqual :: Point -> Point -> Bool
// isEqual :: (Point, Point) -> Bool
export const isEqual = curry((p1, p2) => p1.x === p2.x && p1.y === p2.y);
