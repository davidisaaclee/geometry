import * as R from 'ramda';
import * as Point from './point';

const { curry } = R;

// Rect ::= { origin :: Point, size :: Point }
// SpreadRect ::= { x, y, width, height }
// Rectlike ::= Rect | SpreadRect


export const zero = {
	origin: Point.zero,
	size: Point.zero,
};

// toRect :: a -> Rect?
export const toRect = (rectlike) => {
	if (isRect(rectlike)) {
		return rectlike;
	} else if (isSpreadRect(rectlike)) {
		const { x, y, width, height } = rectlike;
		return { origin: { x, y }, size: { x: width, y: height } };
	} else if (isClientRect(rectlike)) {
		const { left: x, top: y, width, height } = rectlike;
		return { origin: { x, y }, size: { x: width, y: height } };
	} else {
		return undefined;
	}
};

// mapRectlike :: (Rect -> a) -> Rectlike -> a
const mapRectlike = curry((transform, rectlike) => {
	const rect = toRect(rectlike);

	if (rect != null) {
		return transform(rect);
	} else {
		return undefined;
	}
});

// fitToPoints :: [Point] -> Rect
export const fitToPoints = (points) => {
	const { min, max } = points.reduce(({ min: prevMin, max: prevMax }, pt) => ({
		min: {
			x: pt.x < prevMin.x ? pt.x : prevMin.x,
			y: pt.y < prevMin.y ? pt.y : prevMin.y,
		},
		max: {
			x: pt.x > prevMax.x ? pt.x : prevMax.x,
			y: pt.y > prevMax.y ? pt.y : prevMax.y,
		}
	}), { min: { x: Infinity, y: Infinity }, max: Point.zero });

	return {
		origin: min,
		size: Point.subtract(max, min)
	};
}

// transform :: Transform -> Rectlike -> Rect
export const transform = curry((matrix, rectlike) =>
	mapRectlike(({ origin, size }) => {
		const corners = [
			origin,
			Point.add(origin)({ x: size.x, y: 0 }),
			Point.add(origin)({ x: 0, y: size.y }),
			Point.add(origin)(size),
		];

		const transformedCorners =
			corners.map(Point.transform(matrix));

		return fitToPoints(transformedCorners);
	})(rectlike));


// Returns true if `possiblyRect` has shape { origin :: Point, size :: Point }
const isRect = (possiblyRect) => 
	('origin' in possiblyRect) && ('size' in possiblyRect) 
		&& isPoint(possiblyRect.origin) && isPoint(possiblyRect.size);

// Returns true if `possiblySpreadRect` has shape { x, y, width, height }
const isSpreadRect = (possiblySpreadRect) =>
	('x' in possiblySpreadRect) && ('y' in possiblySpreadRect) 
		&& ('width' in possiblySpreadRect) && ('height' in possiblySpreadRect);

// Returns true if `possiblyClientRect` has shape { left, top, width, height }
const isClientRect = (possiblyClientRect) =>
	['left', 'top', 'width', 'height'].every((key) => key in possiblyClientRect);

// Returns true if `possiblyPoint` has shape { x, y }
const isPoint = (possiblyPoint) =>
	('x' in possiblyPoint) && ('y' in possiblyPoint);

// Reduces the input rect by the input inset.
// Use negative insets to make rect larger.
// insetRect :: Point -> Rectlike -> Rect
export const insetRect = R.uncurryN(2, (insets) => 
	mapRectlike(({ origin, size }) => ({
		origin: Point.add(origin)(Point.scale(0.5)(insets)),
		size: Point.subtract(size)(insets)
	})));

// toXYWidthHeight :: Rect -> { x, y, width, height }
export const toXYWidthHeight = ({origin, size}) => ({
	x: origin.x,
	y: origin.y,
	width: size.x,
	height: size.y
});

// Returns an array of the rect's corners ([top-left, top-right, bottom-right, bottom-left]).
// corners :: Rect -> [Point]
export const corners = (rect) => [
	rect.origin,
	{ x: rect.origin.x + rect.size.x, y: rect.origin.y },
	{ x: rect.origin.x + rect.size.x, y: rect.origin.y + rect.size.y },
	{ x: rect.origin.x, y: rect.origin.y + rect.size.y},
];

// isEqual :: Rect -> Rect -> Bool
// isEqual :: (Rect, Rect) -> Bool
export const isEqual = curry((rect1, rect2) =>
	Point.isEqual(rect1.origin, rect2.origin) && Point.isEqual(rect1.size, rect2.size));

