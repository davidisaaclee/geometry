import nudged from 'nudged';
import * as Matrix from 'matrix';

// Transform ::= Number[3][3]

// identity :: Transform
export const identity = [
	[1, 0, 0],
	[0, 1, 0],
	[0, 0, 1],
];

// rotation :: angle -> Transform
export const rotation = (angle) => [
	[Math.cos(angle), -Math.sin(angle), 0],
	[Math.sin(angle), Math.cos(angle), 0],
	[0, 0, 1]
];

// scaling :: Point -> Transform
export const scaling = ({x: sx, y: sy}) => [
	[sx, 0, 0],
	[0, sy, 0],
	[0, 0, 1]
];

// translation :: Point -> Transform
export const translation = ({x: tx, y: ty}) => [
	[1, 0, tx],
	[0, 1, ty],
	[0, 0, 1]
];

// invert :: Transform -> Transform?
export const invert = Matrix.invert;

// concatenate :: Transform -> Transform -> Transform
//              | (Transform, Transform) -> Transform
export const concatenate = Matrix.multiply;

// concatenateAll :: [Transform] -> Transform
// concatenateAll([a, b, c]) == concatenate(a, concatenate(b, c))
export const concatenateAll = (transforms) =>
	transforms.reduceRight((acc, elm) => concatenate(elm, acc), identity);

// SVGTransform ::= 'transform(<a>,<b>,<c>,<d>,<e>,<f>)'
// svgTransformFrom :: Transform -> SVGTransform
export const svgTransformFrom = (matrix) =>
	`matrix(${matrix[0][0]},${matrix[1][0]},${matrix[0][1]},${matrix[1][1]},${matrix[0][2]},${matrix[1][2]})`;

// projecting :: { from :: [Point], to :: [Point] } -> Transform
export const projecting = ({ from, to }) => {
	const toArrayPoint = ({ x, y }) => [x, y];
	const {a, b, c, d, e, f} = nudged.estimate('TSR', from.map(toArrayPoint), to.map(toArrayPoint)).getMatrix();
	return [
		[a, c, e],
		[b, d, f],
		[0, 0, 1]
	];
};


// translationFromAffineTransform :: Transform -> Point
export function translationFromAffineTransform(transform) {
	return {
		x: transform[0][2],
		y: transform[1][2],
	};
}

// Calculates the rotation angle in radians from an affine transform.
// rotationFromAffineTransform :: Transform -> Number
export function rotationFromAffineTransform(transform) {
	return Math.atan2(transform[1][0], transform[1][1]);
}

// scaleFromAffineTransform :: Transform -> Point
export function scaleFromAffineTransform(transform) {
	return {
		x: Math.sqrt(transform[0][0] * transform[0][0] + transform[1][0] * transform[1][0]),
		y: Math.sqrt(transform[0][1] * transform[0][1] + transform[1][1] * transform[1][1]),
	};
}

// tsrFromTransform :: Transform -> { translation :: Point, scale :: Point, rotation :: Number }
export function tsrFromTransform(transform) {
	return {
		translation: translationFromAffineTransform(transform),
		scale: scaleFromAffineTransform(transform),
		rotation: rotationFromAffineTransform(transform),
	};
}

