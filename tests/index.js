import assert from 'assert';
import { Point, Rect, Transform } from '../dist';

describe('Point', () => {
	it('should support translation', () => {
		assert.deepEqual(Point.translate({ x: 5, y: 5 })({ x: 1, y: -1 }), { x: 6, y: 4 });
	});
});

describe('Rect', () => {
	it('should be able to build a zero rect', () => {
		assert.deepEqual(Rect.zero, { origin: { x: 0, y: 0 }, size: { x: 0, y: 0 } });
	});
});

describe('Transform', () => {
	it('should be able to concatenate', () => {
		const xf = Transform.concatenate(
			Transform.translation({ x: 5, y: 5 }),
			Transform.translation({ x: 1, y: -1 }));

		assert.deepEqual(xf, Transform.translation({ x: 6, y: 4 }));
	});
});

